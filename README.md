# Water in bubble CSS animation


![alt text][logo]

[logo]: water.gif "water bubble gif"


- This animation doesn't use any Javascript
- I used SVG that I created with [Adobe XD](https://www.adobe.com/be_fr/products/xd.html)
- The prototype also is in the repo.